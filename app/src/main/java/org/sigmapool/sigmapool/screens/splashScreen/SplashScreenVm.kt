package org.sigmapool.sigmapool.screens.splashScreen

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import org.kodein.di.generic.instance
import org.sigmapool.sigmapool.App.Companion.kodein
import org.sigmapool.sigmapool.provider.coin.ICoinProvider

class SplashScreenVm(val view: ISplashScreenVIew) : ViewModel() {

    val coinProvider by kodein.instance<ICoinProvider>()

    val loadingVisibility = MutableLiveData(true)
    val errorVisibility = MutableLiveData(false)

    init {
        getInitData()
    }

    fun onRefresh() {
        showLoading()
        getInitData()
    }

    private fun showLoading() {
        loadingVisibility.postValue(true)
        errorVisibility.postValue(false)
    }

    private fun showError() {
        loadingVisibility.postValue(false)
        errorVisibility.postValue(true)
    }

    private fun getInitData() {
        GlobalScope.launch(Dispatchers.IO) {
            var coinResult = true
            val coinDeferred = async { coinResult = coinProvider.init() }

            coinDeferred.await()
            if (coinResult) {
                view.startBaseApp()
            } else {
                showError()
            }
        }
    }
}