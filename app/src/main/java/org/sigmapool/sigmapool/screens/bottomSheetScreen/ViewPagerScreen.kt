package org.sigmapool.sigmapool.screens.bottomSheetScreen

class ViewPagerScreen(
    val position: Int,
    val smoothScroll: Boolean = true
)