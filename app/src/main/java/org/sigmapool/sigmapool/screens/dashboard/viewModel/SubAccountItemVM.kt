package org.sigmapool.sigmapool.screens.dashboard.viewModel

class SubAccountItemVM(
    val name: String = "",
    val hashrate: String = "",
    val coinValue: String = "",
    val coin: String = ""
)