package org.sigmapool.sigmapool.screens.poolInfo.params

class PoolInfoItemParams(
    val coinLabel: String,
    val hashLabel: String
)
