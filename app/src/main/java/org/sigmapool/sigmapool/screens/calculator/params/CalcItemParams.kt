package org.sigmapool.sigmapool.screens.calculator.params

class CalcItemParams(
    val coinLabel: String,
    val hashLabel: String
)
